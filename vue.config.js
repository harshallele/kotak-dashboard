module.exports = {
  publicPath: ""
  ,devServer: {
    proxy: {
      '/offers': {
        target: 'http://localhost:3000',
        changeOrigin: true
      }
    }
  }
}