import Vue from 'vue'
import Router from 'vue-router'
import Dashboard from '@/modules/dashboard/dashboard'

Vue.use(Router)

var routes = [{
  path: '/',
  name: 'dashboard',
  component: Dashboard
}]

const routeList = [
];

routeList.map((r) => routes.push(r));

export default new Router({
  routes
})