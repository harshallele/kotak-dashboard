import axios from "axios";
import cfg from "../config";

const baseUrl = cfg.baseUrlContextPath;

const getOffersREST = {
  getRecords(tenor, options) {
    options = options || {};
    options.method = options.method||((cfg.jsonserver)?"GET":"POST");
    options.data = options.data || {};
    const url = (cfg.jsonserver)?("/offers"):(baseUrl+"REST/offers/read"); 
    return this.requestCall(url, options);
  },
  requestCall(url, options) {
    options.credentials = "same-origin";
    return axios(url, options)
      .then(response => {
        return response.data;
      })
      .catch(err => {
        return err;
      });
  }
};

export default getOffersREST;
